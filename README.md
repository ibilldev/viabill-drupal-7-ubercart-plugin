### DRUPAL UBERCART  : ViaBill ePay Module ###
----------------------  

ViaBill has developed a free payment module using Epay Payment Gateway which enables your customers to pay online for their orders in your Drupal Ubercart Web Shop.
ViaBill is a Payment Method and not a Payment Gateway.

###Facts###
---------
- version: 1.1
- Plugin on BitBucket (https://pdviabill@bitbucket.org/ibilldev/viabill-drupal-7-ubercart.git)


###Description###
-----------
Pay using ViaBill. 
Install this Plugin in to your Ubercart Web Shop to provide a separate payment option to pay using ViaBill.

#Requirements
------------
* PHP >= 5.2.0

#Compatibility
-------------
* Drupal 7.0
* Ubercart > 2.0


###Integration Instructions###
-------------------------
1. Download the Module from the bitbucket. 

2. Module contains one folder a.) uc_viabillepay

3. Extract the folder uc_viabillepay and place it inside  DrupalProject/sites/all/modules/ 
								
			            OR

4. Instal using the Module Installation :  Admin->Modules->Install. 
   Select the zip foler uc_viabillepay.zip and upload. 
![drupal-ubercart-install.PNG](https://bitbucket.org/repo/gqzk9n/images/3739276558-drupal-ubercart-install.PNG)

5. Admin Panel- Choose Modules in the menu (please see image 1 below). Find and check the box by the ViaBill ePay module (in Ubercart - Payment), and press ‘Save configuration’ at the bottom. If it asks if you want to install the module, click on ‘Continue’.
![drupal-ubercart-modules-check.PNG](https://bitbucket.org/repo/gqzk9n/images/3223600829-drupal-ubercart-modules-check.PNG)

6. Go to Store -> Configuration -> Payment methods. Press ‘Settings’ by the ViaBillePay module.
![drupal-ubercart-payment-settings.PNG](https://bitbucket.org/repo/gqzk9n/images/1273570692-drupal-ubercart-payment-settings.PNG)

7. Enter your merchant number in the field “merchant number”. It’s either a test or a production merchant number. 

8. Enter the Price Tag Script received from ViaBill. 

9. Press ‘Save configuration’ at the bottom.

8. Done.


##Uninstallation/Disable Module
-----------------------
1. Go to the Admin->Modules
2. Look For "ViaBill ePay "
3. Uncheck the Payment Method and click Save Configuration.
4. Go to Admin->Modules->Uninstall
5. Select ViaBillePay module and click Uninstall.
6. Now we must delete the files physically from the folder they are stored in using FTP, SSH or your cPanel.


#Support
-------
If you have any issues with this extension, kindly drop us a mail on [support@viabill.com](mailto:support@viabill.com)

#Contribution
------------


#License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)
