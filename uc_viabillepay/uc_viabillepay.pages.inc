<?php
/*
  Copyright (c) 2016. All rights reserved ViaBill - www.viabill.com

  This program is free software. You are allowed to use the software but NOT allowed to modify the software. 
  It is also not legal to do any changes to the software and distribute it in your own name / brand. 
*/
 
//
// Retrieves the GET parameters from ePay Gateway
// 
function getAcceptViabillepayParam($name)
{
	$res = '';
	if(isset($_GET[$name]))
	{
		$res = $_GET[$name];
	}
	return $res;
}

function curPageURL()
{
	$pageURL = 'http';
	if(isset($_SERVER["HTTPS"]))
	{
		if($_SERVER["HTTPS"] == "on")
		{
			$pageURL .= "s";
		}
	}
	$pageURL .= "://";
	if($_SERVER["SERVER_PORT"] != "80")
	{
		$pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
	}
	else
	{
		$pageURL .= $_SERVER["SERVER_NAME"] . "." . $_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

function remove_querystring_var($url, $key)
{
	$url = preg_replace('/(.*)(?|&)' . $key . '=[^&]+?(&)(.*)/i', '$1$2$4', $url . '&');
	$url = substr($url, 0,  - 1);
	return $url;
}

function uc_viabillepay_complete($cart_id = 0)
{
	$callback = 0;
	
	//
	// Find out if the request is callback
	//
	if(((int)getAcceptViabillepayParam('callback')) == 1)
	{
		$callback = 1;
	}
	watchdog('viabillepay', 'Receiving new order notification for order !order_id.', array('!order_id' => check_plain(getAcceptViabillepayParam('orderid')), ));
	
	//
	// Load the order id - fetch from GET string parameters
	//
	$order = uc_order_load(getAcceptViabillepayParam('orderid'));
	
	//
	// First fetch the ViaBill ePay data (orderid already fetched above)
	//
	$tid = getAcceptViabillepayParam('txnid');
	$orderid = $amount = getAcceptViabillepayParam('orderid');
	$amount = getAcceptViabillepayParam('amount');
	$currency = getAcceptViabillepayParam('currency');
	$date = getAcceptViabillepayParam('date');
	$hash = getAcceptViabillepayParam('hash');
	$fraud = getAcceptViabillepayParam('fraud');
	$cardid = getAcceptViabillepayParam('paymenttype');
	$postfix = getAcceptViabillepayParam('cardno');
	$transfee = getAcceptViabillepayParam('txnfee');
	
	
	//
	// Validate if transactionid was received from ePay
	//
	if($tid < 0)
	{
		print t('An error has occurred during payment. No order id was provided. Please contact us to ensure your order has submitted.');
		exit();
	}
	
	//
	// Validate the MD5 stamp (if used)
	//
	if(strlen(variable_get('uc_viabillepay_md5_password ', 0)) > 0)
	{
		$accept_params = $_GET;
		$var = "";
		foreach ($accept_params as $key => $value)
		{
			if($key != "hash" and $key != "q")
				$var .= $value;
		}
		
		$md5key_password = variable_get('uc_viabillepay_md5_password', '');
		if($hash != md5($var . $md5key_password))
		{
			print t('Error in MD5 data - key received from ePay does not match the internal generated key! No order id was provided. Please contact us to ensure your order has submitted.');
			exit();
		}
	}
	
	//
	// Only do the order processing on callback from ePay
	//
	if($callback == 1)
	{
		//
		// Add ePay information to the order
		//
		$path = base_path() . drupal_get_path('module', 'uc_viabillepay');
		
		$comment = 'Payment is approval at ViaBill ePay.dk.<br>';
		$comment .= '<table border="0">';
		$comment .= '<tr><td><b>ViaBill ePay transaction id:</b></td><td>' . $tid . '</td></tr>';
		$comment .= '<tr><td><b>Card postfix:</b></td><td>' . $postfix . '</td></tr>';
		$comment .= '<tr><td><b>ViaBill ePay order id:</b></td><td>' . $orderid . '</td></tr>';
		$comment .= '<tr><td><b>ViaBill ePay amount:</b></td><td>' . number_format($amount / 100, 2, ',', '.') . '</td></tr>';
		$comment .= '<tr><td><b>ViaBill ePay currency:</b></td><td>' . $currency . '</td></tr>';
		$comment .= '<tr><td><b>ViaBill ePay auth-date:</b></td><td>' . $date . '</td></tr>';
		if($fraud > 0)
		{
			$comment .= '<tr><td><b>ViaBill ePay fraudstatus:</b></td><td>' . $fraud . '</td></tr>';
		}
		
		$comment .= '<tr><td><b>ViaBill ePay cardtype:</b></td><td><img src="' . $path . '/images/' . $cardid . '.png" border="0"></td></tr>';
		if($transfee > 0)
		{
			$comment .= '<tr><td><b>ViaBill ePay transaction fee:</b></td><td>' . $transfee . '</td></tr>';
		}
		
		$comment .= '</table>';
		$comment .= '<a href="https://ssl.ditonlinebetalingssystem.dk/admin/" target="_blank">' . t('click here to process the payment at the gateway') . '</a>';
		uc_order_comment_save($order->order_id, 0, $comment, 'admin');
		uc_payment_enter($order->order_id, 'ViaBillePay', $amount / 100, 0, NULL, 'Payment completed via ViaBill ePay');
	}
	
	//
	// Now redirect to the order completed page - and finalize the order
	//
	$url = 'cart/viabillepay/finalize/' . $order->order_id;
	//
	// Javascript redirect on the finalization page.
	//
	$output = '<script type="text/javascript">window.location = "' . url($url, array('absolute' => TRUE, )) . '"</script>';
	
	//
	// Text link for users without Javascript enabled.
	//
	$output .= l(t('Click to complete checkout.'), $url, array('absolute' => TRUE, ));
	
	print $output;
	exit();
}

//
// Formats to minor umits 
//
function trimAmount($amount)
{
	return ((float)$amount) * 100;
}

//
// Converts from ISO currency code to name
//
function getCurrencyName($currencyCode)
{
	switch($currencyCode)
	{
		case '036':
			return t('AUD');
		case '124':
			return t('CAD');
		case '208':
			return t('DKK');
		case '344':
			return t('HKD');
		case '352':
			return t('ISK');
		case '392':
			return t('JPY');
		case '484':
			return t('MXN');
		case '554':
			return t('NZD');
		case '578':
			return t('NOK');
		case '702':
			return t('SGD');
		case '710':
			return t('ZAR');
		case '752':
			return t('SEK');
		case '756':
			return t('CHF');
		case '826':
			return t('GBP');
		case '840':
			return t('USD');
		case '949':
			return t('TRY');
		case '978':
			return t('EUR');
		case '985':
			return t('PLN');
	}
	return '<font color="red">!ERROR IN CURRENCY!</font>';
}

//
// Called when the payment is completed
//
function uc_viabillepay_finalize()
{
	$order = uc_order_load(arg(3));
	
	// Add a comment to let sales team know this came in through the site.
	uc_order_comment_save($order->order_id, 0, t('Order created through website.'), 'admin');
	
	$output = drupal_render(uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE)));
	
	$page = variable_get('uc_cart_checkout_complete_page', '');
	
	if(!empty($page))
	{
		drupal_goto($page);
	}
	
	return $output;
}

